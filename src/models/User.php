<?php
namespace Models;

use \Models\Database;

class User
{
  private $db;

  public function __construct()
  {
    $this->db = new Database;
  }

  public function getRowByUserAndPassword($username,$password)
  {
    $username = filter_var($username, FILTER_SANITIZE_STRING);
    $password = filter_var($password, FILTER_SANITIZE_STRING);

    $sql=""
    ."SELECT * "
    ." FROM usuario "
    ." WHERE nombreUsuario = :username AND clave =  :password"
    .""; 

    $this->db->query($sql);

    $this->db->bind(':username', $username);
    $this->db->bind(':password', $password);
    return $this->db->resultSet();
  }

}
